package org.bitbucket.woolmark;

class WoolMarkConstant {
	/** The height of AWT frame title. */
	public static final int AWT_FRAME_TITLE_HEIGHT = 23;

	/** The merging of AWT frame window. */
	public static final int AWT_FRAME_WINDOW_MERGIN = 4;

	/** The width of the window */
	public static final int WIDTH = 120;

	/** The height of window */
	public static final int HIGHT = 120;


	/** The interval of updating the window. The unit is ms */
	public static final int SPEED = 60;

	/** The maximum number of sheep. */
	public static final int SHEEP_MAX = 100;
}
