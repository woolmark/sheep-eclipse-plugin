package org.bitbucket.woolmark;

import java.util.Random;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class WoolMarkCanvas extends Canvas implements Runnable {
	private final Color SKY_COLOR;
	private final Color GROUND_COLOR;
	private final Color TEXT_COLOR;

	private Thread runner;
	private static Random rand;
	private Image[] sheep_image;
	private Image fence;
	private int[][] sheep_pos = new int[WoolMarkConstant.SHEEP_MAX][4];
	public boolean flag = false;
	private int sheep_number;
	private int sheepState;
	private GC g;
	private Display display;

	public WoolMarkCanvas(Composite parent) {
		super(parent, SWT.NONE);
		setSize(120, 120);
		this.display = getDisplay();
		rand = new Random();
		sheep_number = 1;
		sheepState = 0;

		g = new GC(this);
		GROUND_COLOR = new Color(display, 100, 255, 100);
		SKY_COLOR = new Color(display, 150, 150, 255);
		TEXT_COLOR = new Color(display, 0, 0, 0);
		ImageData imageData = null;
		imageData = new ImageData(getClass().getResourceAsStream("/org/bitbucket/woolmark/fence.png"));
		fence = new Image(Display.getCurrent(), imageData);
		sheep_image = new Image[2];
		imageData = new ImageData(getClass().getResourceAsStream("/org/bitbucket/woolmark/sheep00.png"));
		sheep_image[0] = new Image(Display.getCurrent(), imageData);
		imageData = new ImageData(getClass().getResourceAsStream("/org/bitbucket/woolmark/sheep01.png"));
		sheep_image[1] = new Image(Display.getCurrent(), imageData);

		for (int i = 0; i < sheep_pos.length; i++) {
			clearSheepPosition(sheep_pos[i]);
		}

		setSheepInitPosition(sheep_pos[0]);

		runner = new Thread(this);
		runner.start();
		addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent arg0) {
			}

			public void mouseDown(MouseEvent arg0) {
				flag = true;

			}

			public void mouseUp(MouseEvent arg0) {
				flag = false;

			}
		});
	}

	private void paint() {

		g.setBackground(GROUND_COLOR);
		g.fillRectangle(0, 0, WoolMarkConstant.WIDTH, WoolMarkConstant.HIGHT);
		g.setBackground(SKY_COLOR);
		g.fillRectangle(0, 0, WoolMarkConstant.WIDTH, WoolMarkConstant.HIGHT - 70);

		g.drawImage(fence, 35, WoolMarkConstant.HIGHT - 80);

		for (int i = 0; i < sheep_pos.length; i++) {
			if (sheep_pos[i][1] >= 0) {
				g.drawImage(sheep_image[sheepState % 2], sheep_pos[i][0], sheep_pos[i][1]);
			}
		}

		g.setFont(null);
		g.setForeground(TEXT_COLOR);
		g.drawString("羊が" + sheep_number + "匹", 5, 15);
		g.drawString("羊が" + sheep_number + "匹", 5, 15);
	}

	public void run() {
		while (true) {
			calc();
			if (!display.isDisposed()) {
				display.asyncExec(new Runnable() {
					public void run() {
						paint();
					}
				});
			}
			try {
				Thread.sleep(WoolMarkConstant.SPEED);
			} catch (Exception exception) {
			}
		}
	}

	private void addSheep() {
		for (int i = 1; i < sheep_pos.length; i++) {
			if (sheep_pos[i][1] == -1) {
				setSheepInitPosition(sheep_pos[i]);
				return;
			}
		}
	}

	private int getJumpX(int y) {
		y -= (WoolMarkConstant.HIGHT - 40);
		return (70 - 3 * y / 4);
	}

	private void clearSheepPosition(int[] pos) {
		pos[0] = 0;
		pos[1] = -1;
		pos[2] = 0;
		pos[3] = 0;
	}

	private void setSheepInitPosition(int[] pos) {
		pos[0] = WoolMarkConstant.WIDTH - 20;
		pos[1] = (WoolMarkConstant.HIGHT - 40) + rand.nextInt() % 30;
		pos[2] = 0;
		pos[3] = getJumpX(pos[1]);
	}

	private void calc() {
		if (flag) {
			addSheep();
		}

		for (int i = 0; i < sheep_pos.length; i++) {
			if (sheep_pos[i][1] >= 0) {
				sheep_pos[i][0] -= 5;
				if ((sheep_pos[i][3] - 20) < sheep_pos[i][0] && sheep_pos[i][0] <= sheep_pos[i][3]) {
					sheep_pos[i][1] -= 3;
				}

				else {
					if ((sheep_pos[i][3] - 40) < sheep_pos[i][0] && sheep_pos[i][0] <= (sheep_pos[i][3] - 20)) {
						sheep_pos[i][1] += 3;
					}
					if (sheep_pos[i][0] < (sheep_pos[i][3] - 10) && sheep_pos[i][2] == 0) {
						sheep_number++;
						sheep_pos[i][2] = 1;
					}
				}

				if (sheep_pos[i][0] < -20) {
					if (i == 0) {
						setSheepInitPosition(sheep_pos[0]);
					} else {
						clearSheepPosition(sheep_pos[i]);
					}
				}
			}
		}
		sheepState++;
	}

}
