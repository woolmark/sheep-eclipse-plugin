package org.bitbucket.woolmark.views;

import org.bitbucket.woolmark.WoolMarkCanvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;



public class WoolmarkView extends ViewPart {

	@Override
	public void createPartControl(Composite arg0) {
		new WoolMarkCanvas(arg0);
	}

	@Override
	public void setFocus() {
	}

}